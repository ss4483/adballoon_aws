require 'test_helper'

class HashtagTest < ActiveSupport::TestCase
  def setup
    @hashtag = Hashtag.new
    @ad = Ad.new 
    @ad.ad_type = "event"
    @ad.company = "any"
    @ad.phone = "01098804483"
    @ad.title = "title"
    @ad.detail_prize = ["1등", "2등"]
    @ad.link = "http://example.com"
    @ad.start_date = Time.now
    @ad.end_date = Time.now + 1.day
    @ad.img = "aaa.jpg"
    @ad.businessLicenseNumber = 000000
    @ad.save
    @ad.hashtags << @hashtag
  end
 

end
