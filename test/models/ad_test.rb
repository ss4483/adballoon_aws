require 'test_helper'

class AdTest < ActiveSupport::TestCase
  def setup
    @ad = ads(:event)
  end
  
  test 'should be valid' do
    assert @ad.valid?
  end

  test 'name should be present' do 
    @ad.title = '        '
    assert_not @ad.valid?
  end
  # test 'ad_type validation should accept valid addresses' do 
  #   ad_types = %w[event coupon]
  #   @ad.method = "method"
  #   @ad.detail_method = ['1', '2']
  #   @ad.announced_link = "https://example.com"
  #   @ad.announced_date = Time.now
  #   ad_types.each do |ad_type|
  #     @ad.ad_type = ad_type
  #     @ad.etc_message = "aa"
  #     assert @ad.valid?, "#{ad_type.inspect} should be valid"
  #   end
  # end

  # test 'ad_type validation should reject valid addresses' do 
  #   ad_types = [" event", "coupon ","event dd", "s event", "coupon d", "ee coupon"]
  #   ad_types.each do |ad_type|
  #     @ad.ad_type = ad_type
  #     assert_not @ad.valid?, "#{ad_type.inspect} should be invalid"
  #   end
  # end

end
