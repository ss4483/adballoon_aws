module ApplicationHelper
  def recommends
    Ad.where("is_ok = ? AND start_date <= ? AND end_date >= ? AND recommend_date IS NOT NULL", 
              true, Date.current, Date.current).
              select { |ad| ad.recommend_date >= Date.current - (ad.recommend_frequency == 0 ? 7 : 30).days }
  end
end