module ManagersHelper

  def calendar(id)
    color = String.new
    case id.to_i % 5
    when 1 
      color = "#ffa2a2" 
    when 2 
      color = "#a2b2ff"
    when 3 
      color = "#ceffa2"
    when 4 
      color = "#f9ffa2"
    when 0
      color = "#ffdaa2"
    end
    return color
  end
end
