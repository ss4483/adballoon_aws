module MainHelper
  def getBanners
    result = Array.new
    banner_ads = Ad.where("is_ok = 't' AND start_date <= ? AND end_date >= ? AND banner_frequency IS NOT NULL AND banner_date <= ?", Date.current, Date.current, Date.current)
    banner_ads.each do |ad|
      if ad.banner_frequency == 0
        if ad.banner_date + 6.days >= Date.current
          result.push(ad)
        end
      elsif ad.banner_frequency == 1
        if ad.banner_date + 29.days >= Date.current
          result.push(ad)
        end
      end
    end
    
    return result
  end
end
