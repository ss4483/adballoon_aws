module LikesHelper
  def like_it?(ad)
    if user_signed_in?
      icon = Like.find_by(user: current_user, ad: ad).nil? ? 'icon/like.png' : 'icon/unlike.png'
      alt = Like.find_by(user: current_user, ad: ad).nil? ? 'like-'+ad.id.to_s : 'unlike-'+ad.id.to_s
      image_tag icon, alt: alt, height:'25px', style: "cursor:pointer;", class: "like_ad"
    else
      image_tag 'icon/like.png', alt: ad.id, height:'25px', style: "cursor:pointer;", onClick: "alert('로그인이 필요합니다.')"
    end
  end
end
