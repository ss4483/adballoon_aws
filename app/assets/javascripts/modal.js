  /* modal */
  $(document).on({
    "shown.bs.modal": function(e) {
      var button = $(e.relatedTarget);
      var recipient = button.data('whatever');

      var ad_type = recipient.split('/')[0];
      var id = recipient.split('/')[1];
      $.ajax({
        type: 'get',
        url: '/modal_content/'+ ad_type + '/' + id,
        dataType: 'html',
        success: function(data) {
          $('#loading').css("display", "none");
          $("#my-modal-body").html(data);
        }
      });
    }
  }, "#adModal");
  
  $(document).on({
    "hidden.bs.modal": function(e) {
      $('#loading').css("display", "block");
      $("#my-modal-body").children().first().remove();
    }
  }, "#adModal");

  $(document).on({
    "click": function(e) {
      $('#loading').css("display", "block");
      $("#my-modal-body").children().first().remove();
      
      var ad_type = e.target.id.split('/')[0];
      var id = e.target.id.split('/')[1];

      $.ajax({
        type: 'get',
        url: '/modal_content/'+ ad_type + '/' + id,
        dataType: 'html',
        success: function(data) {
          $('#loading').css("display", "none");
          $("#my-modal-body").html(data);
        }
      });
    }
  }, ".sub_ad");
  /* /modal/ */