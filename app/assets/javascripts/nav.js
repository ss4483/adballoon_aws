$(document).on({
  "click": function(e) {
    if ($('.nav-icon-search').hasClass('nav-search-cancle')) {
      $('.nav-search-bar form').removeClass('nav-search-bar-in-effect');
      $('.nav-search-bar form').addClass('nav-search-bar-out-effect');
      $('.nav-icon-search').addClass("nav-item-none-effect");
      setTimeout(function() {
        $('.nav-icon-search').removeClass("nav-item-none-effect");
        $('.nav-search-bar').attr('style', 'display:none !important;');
        $('.search-bg').css('display','none');
        $('.nav-d-day').removeAttr("style");
        $('.nav-market').removeAttr("style");
        $('.nav-client').removeAttr("style");
        $('.nav-login').removeAttr("style");
        $('.nav-d-day').removeClass("nav-item-none-effect");
        $('.nav-market').removeClass("nav-item-none-effect");
        $('.nav-client').removeClass("nav-item-none-effect");
        $('.nav-login').removeClass("nav-item-none-effect");
        $('.nav-d-day').addClass("nav-item-effect");
        $('.nav-market').addClass("nav-item-effect");
        $('.nav-client').addClass("nav-item-effect");
        $('.nav-login').addClass("nav-item-effect");
        $('.nav-icon-search').removeClass('nav-search-cancle');

        $('.recommend_tag').css('display', 'none');
        $('.recommend_tag').css('padding', 0 + 'px');
        $('.recommend_tag').width(0);  
        

      }, 200);
      
    } else {
      $('.nav-search-bar form').removeClass('nav-search-bar-out-effect');

      $('.nav-d-day').removeClass("nav-item-effect");
      $('.nav-market').removeClass("nav-item-effect");
      $('.nav-client').removeClass("nav-item-effect");
      $('.nav-login').removeClass("nav-item-effect");
      $('.search-bg').css('display','block');
      $('.nav-d-day').addClass("nav-item-none-effect");
      $('.nav-market').addClass("nav-item-none-effect");
      $('.nav-client').addClass("nav-item-none-effect");
      $('.nav-login').addClass("nav-item-none-effect");
      $('.nav-icon-search').addClass("nav-item-none-effect");

      setTimeout(function() {
        $('.nav-icon-search').removeClass("nav-item-none-effect");
        $('.nav-d-day').attr('style', 'display:none !important;');
        $('.nav-market').attr('style', 'display:none !important;');
        $('.nav-client').attr('style', 'display:none !important;');
        $('.nav-login').attr('style', 'display:none !important;');
        $('.nav-search-bar').removeAttr("style");
        $('.nav-search-bar form').addClass('nav-search-bar-in-effect');
        $('.nav-icon-search').addClass('nav-search-cancle');
        
        $('.recommend_tag').width($('.nav-search-bar').width());
        $('.recommend_tag').css('padding', 24 + 'px');
        $('.recommend_tag').css('margin-left', $('.nav-search-bar').position().left + 'px');
        $('.recommend_tag').css('display', 'block');

        $('.nav-search-bar form input').focus(); 
      }, 200);
    }
  }
}, ".nav-icon-search");
$(document)
.on({
  "click": function(e) {
    if ($('.nav-icon-search').hasClass('nav-search-cancle') && e.target.nodeName === "DIV") {
      $('.nav-icon-search').click();
    }
  }
}, 'body');

$(document).on({ "mouseenter": function() { $('.nav-d-day a').text('오늘 마감'); }, "mouseleave": function() { $('.nav-d-day a').text('D-DAY'); }  }, '.nav-d-day');
$(document).on({ "mouseenter": function() { $('.nav-market-name').text('애드마켓'); }, "mouseleave": function() { $('.nav-market-name').text('AD MARKET'); } }, '.nav-market');
$(document).on({ "mouseenter": function() { $('.nav-market-advertise').text('광고하기');}, "mouseleave": function() { $('.nav-market-advertise').text('ADVERTISE'); } }, '.nav-client');

$( document ).on('turbolinks:load', function() {
// HASHTAG SEARCH
  $("input.hashtag_input").on("keyup", function(event) {
    if(event.keyCode == 32 || event.keyCode == 13){
      var start = this.selectionStart;
      var end = this.selectionEnd;
    
      var input = $(this);
      var value = input.val();
      var ends_with_space = (value.substr(-1) == " ");

      var hashed_value = "";
      var parts = value.split(" ");
      
      for (var i = 0; i < parts.length; i++) {
          var part = parts[i];
          if (part.indexOf("#") != 0) {
              part = "#" + part;
              start++;
              end++;
          }
          if (part != "#") {
              if (hashed_value == "") {
                  hashed_value = part;
              } else {
                  hashed_value += " " + part;
              }
          }
      }

      if (ends_with_space) hashed_value = hashed_value + " ";

      input.val(hashed_value.replace(",", ""));
      
      this.setSelectionRange(start, end);
      if (event.keyCode == 13) $('.nav-search-form').submit();
    }

  });

});
