$(document).on('click', '.plus',function(event) {
  let $container = $(event.target).parent().next();
  if ($container.children().length < 5) {
    let num = parseInt($($container.children(":last").find('span')).text()) + 1;
    let $temp = $(event.target).parent().next().children(":last").clone();
    $temp.find('span').text(num); // span text 변경 

    let strSpanId = $temp.find('span').attr("id").slice(0, -1) + num;

    $temp.find('span').attr("id", strSpanId);
    $temp.find('input').attr("ria-describedby",strSpanId);
    $temp.find('input').val('');
    $temp.appendTo( $container );
  } else {
    alert('최대 5개까지만 가능합니다.');
  }
});

$(document).on('click', '.minus',function(event) {
  let $container = $(event.target).parent().next();
  if ($container.children().length > 1) {
    $container.children(":last").remove();
  }
});


$(document).on('click', '.option',function(event) {
  $(event.target).parent().parent().find('input').not(".option").prop('disabled', function(i, v) { return !v; });
});

$(document).on('click', '.btn_client_info',function() {
  $(event.target).parent().parent().next().toggle();
  $(event.target).parent().parent().next().next().toggle();
});