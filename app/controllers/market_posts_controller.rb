class MarketPostsController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  before_action :set_market_post, only: [:edit, :update, :destroy]
  
  def new 
    @market_post = MarketPost.new
  end

  def create
    market_post_params = params.require(:market_post).permit(:post_type, :title, :content)
    @market_post = MarketPost.new market_post_params
    @market_post.user = current_user

    if @market_post.save
      redirect_to market_post_path(@market_post), { flash: {success: '등록 되었습니다.'} }
    else
      flash[:danger] = '실패하였습니다.'
      redirect_back(fallback_location: root_path)
    end
  end

  def show
    @market_post = MarketPost.find(params[:id])
  end

  def index
    @recommends = recommends
    if params[:type].nil?
      @market_posts = MarketPost.all
    elsif params[:type] == 'sell'
      @market_posts = MarketPost.where(post_type: "sell")
    elsif params[:type] == 'buy'
      @market_posts = MarketPost.where(post_type: "buy")
    end
    @market_posts = @market_posts.where("#{params[:search_type]} LIKE ?", "%#{params[:serach]}%") if params[:search_type]
    @market_posts = @market_posts.paginate(page: params[:page], per_page: 10) 
  end
  
  def edit
  end

  def update
    market_post_params = params.require(:market_post).permit(:title, :content)
  
    if @market_post.update market_post_params
      redirect_to market_post_path(@market_post), { flash: {success: '수정 되었습니다.'} }
    else
      flash[:danger] = '실패하였습니다.'
      redirect_back(fallback_location: root_path)
    end
  end
  
  def destroy
    @market_post.destroy
    redirect_to market_posts_path, { flash: {success: '삭제 되었습니다.'} }
  end
  
  private
  def set_market_post 
    @market_post = MarketPost.find params[:id]
    redirect_back(fallback_location: root_path, flash: {danger: "권한이 없습니다."}) if (current_user != @market_post .user)
  end

end
