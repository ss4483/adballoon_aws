class ManagersController < ApplicationController
  before_action :manager_check, except: [:my_adballoon]
  before_action :authenticate_user!, only: [:my_adballoon]
  def index
    @ads_count = Ad.all.count
    @users_count = User.all.count
    @banners = Ad.where("banner_frequency IS NOT NULL")
    @recommends = Ad.where("recommend_frequency IS NOT NULL")

    @events = Ad.where(ad_type: "event")
    @coupons = Ad.where(ad_type: "coupon")
  end

  def user_list
    case params[:type]
    when 'client'
      @users = User.where(user_type: "client").paginate(:page => params[:page], :per_page => 10).order("created_at DESC")
    else 
      @users = User.all.paginate(:page => params[:page], :per_page => 10).order("created_at DESC")
    end
  end
  def ad_list
    case params[:type]
    when "total"
      case params[:order]
      when "start_date"
        @ads = Ad.all.order("start_date DESC").paginate(:page => params[:page], :per_page => 10)
      else 
        @ads = Ad.all.order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
      end
    when "none"
      @ads = Ad.where(is_ok: false).paginate(:page => params[:page], :per_page => 10).order("created_at DESC")
    when "img_update"
      @ads = Ad.where("temp_img IS NOT NULL OR temp_banner_img IS NOT NULL").paginate(:page => params[:page], :per_page => 10).order("created_at DESC")
    end
    
  end
  
  def ads_delete
    ad = Ad.find(params[:id])


    ad.destroy
    redirect_back(fallback_location: root_path)
  end

  def img_update
    ad = Ad.find(params[:id])
    if !ad.temp_img.url.nil?
      ad.remote_img_url = @@url + ad.temp_img.url
      ad.remove_temp_img!
      ad.save!
    elsif !ad.temp_banner_img.nil?
      ad.remote_banner_img_url = @@url + ad.temp_banner_img.url
      ad.remove_temp_banner_img!
      ad.save!
    end

    redirect_back(fallback_location: root_path)
  end


  def ad_check
    ad = Ad.find(params[:id])
    ad.is_ok = ad.is_ok ? false : true
    ad.save

    redirect_back(fallback_location: root_path) 
  end

  
  def my_adballoon
    @events = current_user.liked_ads.where(ad_type: "event").paginate(:page => params[:page], :per_page => 10)
    @coupons = current_user.liked_ads.where(ad_type: "coupon").paginate(:page => params[:page], :per_page => 10)
                                             
  end



  def main
    @big_event = Ad.find_by(main_type: 1)
    @big_sale = Ad.find_by(main_type: 2)
    @big_choice = Ad.find_by(main_type: 3)
    # @today_adballoon
  end
  def main_update
    pre_ad = Ad.find_by(main_type: params[:type])
    pre_ad.main_type = 0
    pre_ad.save
    
    after_ad = Ad.find(params[:id])
    after_ad.main_type = params[:type]
    after_ad.save
    
    redirect_back(fallback_location: root_path)
  end

  private
  def manager_check
    if (current_user.user_type != "manager") 
      redirect_to '/', notice: "잘 못 된 접근 입니다."
    end
  end
end
