# frozen_string_literal: true

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def self.provides_callback_for(provider)
    class_eval %Q{
      def #{provider}
        @user = User.find_for_oauth(request.env['omniauth.auth'], current_user)
 
        if @user.persisted?
          # sign_in_and_redirect @user, event: :authentication
          sign_in @user
          @after_sign_in_url = after_sign_in_path_for(@user)
          render '/main/callback', :layout => false
        else
          session["devise.#{provider}_data"] = request.env['omniauth.auth']
          redirect_to @after_sign_in_url
          # redirect_to user_session_path
        end
      end
    }
  end
  
  [:facebook, :naver].each do |provider|
    provides_callback_for provider
  end
  
  def after_sign_in_path_for(resource)
    auth = request.env['omniauth.auth']
    @identity = Identity.find_for_oauth(auth)
    @user = User.find(current_user.id)
    if @user.persisted?
      user_session_path
    else
      '/'
    end
  end
end
