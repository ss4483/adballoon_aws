class LikesController < ApplicationController
  def like_toggle
    like = Like.find_by(user: current_user, ad_id: params[:ad_id])
    if like.nil?
      Like.create(user: current_user, ad_id: params[:ad_id])   
    else
      like.destroy
    end

  end
end
