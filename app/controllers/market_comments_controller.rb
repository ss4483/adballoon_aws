class MarketCommentsController < ApplicationController
  before_action :authenticate_user!
  def create
    @market_comment = MarketComment.new 
    @market_comment.content = params[:marekt_content]
    @market_comment.market_post_id = params[:market_post_id]
    @market_comment.user = current_user

    if @market_comment.save
      redirect_to market_post_path(params[:market_post_id]), { flash: {success: '등록 되었습니다.'} }
    else
      flash[:danger] = '실패하였습니다.'
      redirect_back(fallback_location: root_path)
    end
  end

  def destroy
    @market_comment = MarketComment.find(params[:id])
    if MarketComment.where(reply_comment_id: params[:id]).empty?
      @market_comment.destroy
      redirect_to market_post_path(params[:market_post_id]), { flash: {success: '삭제 되었습니다.'} }
    else
      flash[:danger] = '답글이 있어 삭제가 불가능합니다.'
      redirect_back(fallback_location: root_path)
    end
  end

  def reply
    @market_comment = MarketComment.new 
    @market_comment.content = params[:marekt_content]
    @market_comment.market_post_id = params[:market_post_id]
    @market_comment.reply_comment_id = params[:id]
    @market_comment.user = current_user

    if @market_comment.save
      redirect_to market_post_path(params[:market_post_id]), { flash: {success: '등록 되었습니다.'} }
    else
      flash[:danger] = '실패하였습니다.'
      redirect_back(fallback_location: root_path)
    end
  end
end
