class AdsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_ad, only: [:edit, :update, :destroy]
  before_action :ad_params, only: [:create, :update]
  
  # Create
  def new 
    @ad = Ad.new
  end

  def create
    # debugger
    @ad = Ad.new @ad_params
    @ad.assign_attributes(@banner_option) if params[:banner_option]
    @ad.assign_attributes(@recommend_option) if params[:recommend_option]

    @ad.user = current_user

    if @ad.save
      current_user.user_type = 'client' if current_user.user_type == 'default'
      #current_user.company = @ad.company
      # current_user.businessLicenseNumber = @ad.businessLicenseNumber
      # current_user.phone  = @ad.phone
 

      @hashtags[:name].split('#').reject(&:blank?).each do |tag_name|
        hashtag = Hashtag.find_or_create_by(name: tag_name) 
        @ad.hashtags << hashtag  if not @ad.hashtags.include?(hashtag)
      end
      
      # AlertMailer.ad_create_email(current_user).deliver_now

      redirect_to ads_url
    else
      redirect_to new_ad_url
    end
  end
  
  # Retrieve || Read
  def index 
    @ads = current_user.ads.paginate(:page => params[:page], :per_page => 10)
  end
  
  # Update
  def edit
  end
  
  def update
    if @ad.update @ad_params
      @ad.update(@banner_option)
      @ad.update(@recommend_option)

      redirect_to ads_url
    else
      redirect_to new_ad_url
    end
  end
  
  # Destroy
  def destroy
    @ad.destroy    

    redirect_to ads_url
  end
  
  
  private
  def set_ad 
    @ad = Ad.find params[:id]
    redirect_back(fallback_location: root_path, flash: {danger: "권한이 없습니다."}) if not (current_user == @ad.user)
  end
  
  def ad_params
    if params[:_method].nil?
      if params[:ad_type] == 'event'
        @hashtags = params.require(:event).require(:hashtags).permit(:name)
        @ad_params = params.require(:event).permit(:ad_type,:company, :businessLicenseNumber, :phone,
                                    :method, {detail_method: []} , :announced_link, :announced_date,
                                    :title, {detail_prize: []} , :link, :etc_message, :start_date, :end_date, :img )
                                    
        @banner_option = params.require(:event).permit(:banner_img, :banner_date, :banner_frequency)
        @recommend_option = params.require(:event).permit(:recommend_frequency, :recommend_date)
      elsif params[:ad_type] == 'coupon'
        @hashtags = params.require(:coupon).permit({hashtag: []})
        @ad_params = params.require(:coupon).permit(:ad_type, :company, :businessLicenseNumber, :phone,
                                    :title, {detail_prize: []}, :link, :etc_message, :start_date, :end_date, :img)
                                    
        @banner_option = params.require(:coupon).permit(:banner_img, :banner_date, :banner_frequency)
        @recommend_option = params.require(:coupon).permit(:recommend_frequency, :recommend_date)
      end

    elsif params[:_method] == "patch"
      if @ad.is_ok # 관리자 승인 후 

      else # 관리자 승인 전
        if params[:ad_type] == 'event'
          @hashtags = params.require(:event).permit({hashtag: []})
          @ad_params = params.require(:event).permit(:ad_type, :company, :businessLicenseNumber, :phone,
                                      :title, {detail_prize: []}, :link, :etc_message, :start_date, :end_date, :temp_img)
          
          @banner_option = params[:banner_option] ? params.require(:event).permit(:banner_img, :temp_banner_img, :banner_date, :banner_frequency) : { banner_img: nil, temp_banner_img: nil, banner_date: nil, banner_frequency: nil }
          @recommend_option = params[:recommend_option] ? params.require(:event).permit(:recommend_frequency, :recommend_date) : { recommend_frequency: nil, recommend_date: nil }
        elsif params[:ad_type] == 'coupon'
          @hashtags = params.require(:coupon).permit({hashtag: []})
          @ad_params = params.require(:coupon).permit(:ad_type, :company, :businessLicenseNumber, :phone,
                                      :title, {detail_prize: []}, :link, :etc_message, :start_date, :end_date, :temp_img)
          
          @banner_option = params[:banner_option] ? params.require(:coupon).permit(:banner_img, :temp_banner_img, :banner_date, :banner_frequency) : { banner_img: nil, temp_banner_img: nil, banner_date: nil, banner_frequency: nil }
          @recommend_option = params[:recommend_option] ? params.require(:coupon).permit(:recommend_frequency, :recommend_date) : { recommend_frequency: nil, recommend_date: nil }
        end
      end
    end
  end
end
