class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include ApplicationHelper
  before_action :set_hashtags
  @@url = if Rails.env == "development"
            "http://localhost:3000/"
          else
            ""
            # "http://ec2-52-79-235-144.ap-northeast-2.compute.amazonaws.com/"
          end

  def set_hashtags
    @hashtags = Hashtag.left_joins(:ads).
                        where("is_ok = 't' AND start_date <= ? AND end_date >= ?", Date.current, Date.current).
                        group(:id).order('COUNT(ad_id) DESC').limit(5).
                        map { |custom| { name: custom.name, count: custom.ads.count } }.
                        sort_by { |k| k[:count] }.reverse!
  end
end
