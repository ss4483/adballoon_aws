class MainController < ApplicationController
  include MainHelper
  def index
    @banners = getBanners
    @big_goods = { big_event: Ad.find_by("is_ok = 't' AND start_date <= ? AND end_date >= ? AND main_type = ?", Date.current, Date.current, 1), 
                   big_sale: Ad.find_by("is_ok = 't' AND start_date <= ? AND end_date >= ? AND main_type = ?" , Date.current, Date.current, 2), 
                   big_choice: Ad.find_by("is_ok = 't' AND start_date <= ? AND end_date >= ? AND main_type = ?", Date.current, Date.current, 3), 
                   today_adballoon: Ad.where("is_ok = 't'").sample }
  
    @ads = Ad.where("is_ok = 't' AND start_date <= ? AND end_date >= ?",  Date.current, Date.current).order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
  end

  def modal_content
    @ad = Ad.find(params[:id])
    @ad.views += 1
    @ad.save
    @d_day = (@ad.end_date - DateTime.now.in_time_zone("Seoul").to_date).to_i

    # 관련 광고 가져오기
    
    @sub_ads = Array.new
    tags = @ad.hashtags.pluck(:name)
    tags.shuffle.each do |tagString|
      Hashtag.find_by(name: tagString).ads.shuffle.each do |sub_ad|
        if @sub_ads.length < 4
          @sub_ads << sub_ad unless sub_ad == @ad
        else 
          break
        end
      end
    end
    
    render :layout => false
  end 

  def search
    @recommends = recommends
    
    searches = params[:keyword].delete(' ').split('#').reject(&:empty?)
    
    hash = Hash.new

    searches.each do |search|
     
      hash[search] = Ad.joins(:hashtags).where("is_ok = ? AND start_date <= ? AND end_date >= ? AND hashtags.name = ?", true, Date.current, Date.current, search)
    end
    ads = nil
    hash.values.map { |val| ads ||= val; ads = ads&val }

    @events = ads.select{ |ad| ad.ad_type == "event"}.paginate(:page => params[:page], :per_page => 10)
    @coupons = ads.select{ |ad| ad.ad_type == "coupon"}.paginate(:page => params[:page], :per_page => 10)
    # searches.each do |search|
    #   hash[search] = Array.new
    #   Hashtag.find_by(name: search).ads.each do |tag|
    #     hash[search] << tag.id
    #   end
    # end
    # ad_ids = nil
    # hash.values.map { |val| ad_ids ||= val; ad_ids = ad_ids&val }

    # @events = Ad.where(id: ad_ids).where("is_ok == ? AND ad_type == ? AND start_date <= ? AND end_date >= ?", true,'event', Date.current, Date.current).paginate(:page => params[:page], :per_page => 10)
    # @coupons = Ad.where(id: ad_ids).where("is_ok == ? AND ad_type == ? AND start_date <= ? AND end_date >= ? ", true, 'coupon', Date.current, Date.current).paginate(:page => params[:page], :per_page => 10)


  end

  def d_day
    @recommends = recommends
    @ads = Ad.where("is_ok = ? AND start_date <= ? AND end_date = ?", true, Date.current, Date.current).order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
  end

  def refresh 
    Ad.all.each do |ad|
      ad.start_date = Time.now - (1..5).to_a.sample.days
      ad.end_date = ad.start_date + (7..15).to_a.sample.days
      if !ad.recommend_date.nil?
        ad.recommend_date = ad.start_date + (1..6).to_a.sample.days
      end
      if !ad.banner_frequency.nil?
        ad.banner_date = ad.start_date + (1..6).to_a.sample.days
      end
      ad.save
    end
    
    Ad.all.sample(10).each do |ad|
      ad.is_ok = true
      ad.start_date = Time.now - (1..5).to_a.sample.days
      ad.end_date = Time.now
      ad.save
    end
    User.all.each do |u|
      if !u.ads.empty?
        u.company = u.ads.last.company
        u.businessLicenseNumber = u.ads.last.businessLicenseNumber 
        u.phone = u.ads.last.phone
        u.user_type = "client"
        u.save
      end
    end

    redirect_to '/'
  end
end
