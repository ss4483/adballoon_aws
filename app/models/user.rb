class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  before_create :set_manager

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable
  has_many :ads
  has_many :market_comments
  has_many :market_posts
  has_many :likes
  has_many :liked_ads, through: :likes, source: :ad

  validates :user_type, inclusion: { in: ["default", "client", "manager"] }
  validates :company,
            :businessLicenseNumber,
            :phone, presence: { if: -> { self.user_type == "client" } }



  def self.find_for_oauth(auth, signed_in_resource = nil)
    # user와 identity가 nil이 아니라면 받는다
    identity = Identity.find_for_oauth(auth)
    user = signed_in_resource ? signed_in_resource : identity.user

    # user가 nil이라면 새로 만든다.
    if user.nil?
      # 이미 있는 이메일인지 확인한다.
      email = auth.info.email
      user = User.where(email: email).first
      unless self.where(email: auth.info.email).exists?
        # 없다면 새로운 데이터를 생성한다.
        if user.nil?
          user = User.new(
            email: auth.info.email,
            password: Devise.friendly_token[0,20]
          )
          user.save!
        end
      end
    end
  
    if identity.user != user
      identity.user = user
      identity.save!
    end
    user
  end
  

  private
  def set_manager
    manager_email = ['ss4483@naver.com']
    self.user_type = 'manager' if manager_email.include?(self.email)
  end
end
