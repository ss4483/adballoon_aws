class Like < ApplicationRecord
  belongs_to :ad
  belongs_to :user
  validates_uniqueness_of :ad_id, scope: :user_id

end
