class MarketPost < ApplicationRecord
  mount_uploader :img, ImgUploader
  belongs_to :user
  has_many :market_comments, dependent: :delete_all

  validates :post_type, presence: true,
                        inclusion: { in: ["buy", "sell"] }
  validates :title, presence: true,
                    length: { maximum: 30, minimum: 2 }
  validates :content, presence: true,
                      length: { maximum: 65536, minimum: 10 }
  
  # validates :img

end
