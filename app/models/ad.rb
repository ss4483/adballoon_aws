class Ad < ApplicationRecord
  mount_uploader :img, ImgUploader
  mount_uploader :temp_img, ImgUploader
  mount_uploader :banner_img, ImgUploader
  mount_uploader :temp_banner_img, ImgUploader
     
  # TODO : start_date, end_date, announced_link / banner_date, recommend_date 날짜 유효성 검사
  has_and_belongs_to_many :hashtags
  belongs_to :user
  has_many :likes
  has_many :liked_users, through: :likes, source: :user

  VALID_PHONE_NUMBER_REGEX = /\A010(\d{3}|\d{4})\d{4}\z/
  VALID_URL = URI.regexp


  serialize :detail_method, Array
  serialize :detail_prize, Array

  validates :company, 
            :businessLicenseNumber, 
            :title, 
            :detail_prize, presence: true

  validates :ad_type, presence: true, 
                      inclusion: { in: ["event", "coupon"] }

  validates :phone, presence: true,
                    format: { with: VALID_PHONE_NUMBER_REGEX }
 
  validates :main_type, presence: true,
                        inclusion: { in: [0, 1, 2, 3] }


  validates :link, presence: true,
                   format: { with: VALID_URL }

  validates :method, 
            :detail_method, 
            :announced_link, 
            :announced_date, presence: { if: -> { self.ad_type == "event" } }

  # validates :announced_link, format: { with: VALID_URL }

  validates :etc_message, presence: { if: -> { self.ad_type == "coupon" } }

  # validates :img, presence: true#, format: { with: /.\.(png|jpeg|jpg|gif)\z/ }
  # # :banner_img, :temp_img, :temp_banner_img, if not_nil, format: { with: /.\.(png|jpeg|jpg|gif)\z/ }

  # validates :banner_img, presence: { if: -> { !self.temp_banner_img.nil? } }

  # validates :banner_frequency,
  #           :banner_date, presence: { if: -> { !self.banner_img.nil? } }
  # validates :recommend_frequency,
  #           :recommend_date, presence: { if: -> { !self.banner_img.nil? } }

  validates :banner_frequency, 
            :recommend_frequency, inclusion: { in: [nil, 0, 1] }

  validates :start_date, presence: true
  validates :end_date, presence: true
  validate :end_date_is_after_start_date
  validate :announced_date_is_after_end_date

  
  def detail_method_to_a
    self.detail_method.class.to_s != 'Array' ? JSON.parse(self.detail_method) : self.detail_method
  end  
  def detail_prize_to_a
    self.detail_prize.class.to_s != 'Array' ? JSON.parse(self.detail_prize) : self.detail_prize
  end
  
  def start_time
    self.banner_date
  end 
  def end_time
    if self.banner_frequency == 0
      self.banner_date + 6.days
    else
      if self.banner_date + 29.days >= self.end_date
        self.end_date
      else 
        self.banner_date + 29.days
      end
    end
  end

  private
  def end_date_is_after_start_date
    return if end_date.blank? || start_date.blank?

    if end_date < start_date
      errors.add(:end_date, "cannot be before the start date") 
    end 
  end

  def announced_date_is_after_end_date
    return if announced_date.blank? || end_date.blank?

    if announced_date < end_date
      errors.add(:announced_date, "cannot be before the end date") 
    end 
  end
end
