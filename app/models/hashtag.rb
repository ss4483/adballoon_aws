class Hashtag < ApplicationRecord
  has_and_belongs_to_many :ads

  validates :name, presence: true,
                   uniqueness: true

end
