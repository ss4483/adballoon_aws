class AlertMailer < ApplicationMailer
  default from: 'notifications@example.com'

  # AlertMailer.service_email(current_user).deliver_now
  def service_email(user)
    @user = user
    mail to: User.where(user_type: 'manager').pluck(:email), 
         subject: "[문의사항]#{@user.email}"
  end

  def ad_create_email(user)
    @user = user
    mail to: User.where(user_type: 'manager').pluck(:email), 
         subject: "[등록]#{@user.email}"
  end
  
  def ad_update_email(user)
    @user = user
    mail to: User.where(user_type: 'manager').pluck(:email), 
         subject: "[이미지 수정]#{@user.email}"
  end
end
