CarrierWave.configure do |config|
  config.fog_provider = 'fog/aws'                        # required
  config.fog_credentials = {
    provider:              'AWS',                        # required
    aws_access_key_id:      ENV["ACCESS_KEY"],
    aws_secret_access_key:  ENV["SECRET_KEY"],
    use_iam_profile:       true,                         # optional, defaults to false
    region:                 'ap-northeast-2'
  }
  config.fog_directory = 'ss.portfolio'                                 # required
end 