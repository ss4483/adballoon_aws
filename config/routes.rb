Rails.application.routes.draw do
  get '/refresh' => 'main#refresh'
  root 'main#index'
  get '/modal_content/:type/:id' => 'main#modal_content'

  get '/d_day', to: 'main#d_day'
  get '/search', to: "main#search"
  
  post '/ad_like/:ad_id', to: 'likes#like_toggle'

  resources :market_posts do
    resources :market_comments, only: [:create, :destroy]
  end
  post '/market_posts/:market_post_id/reply/:id', to: 'market_comments#reply', as: 'market_post_reply'


  resources :ads, except: [:show]


  get '/intro', to: 'main#intro'
  
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks'} 

  get 'managers/my_adballoon', to: 'managers#my_adballoon'

  get 'managers/index', to: 'managers#index'
  get 'managers/ad_list', to: 'managers#ad_list'
  get 'managers/user_list', to: 'managers#user_list'
  
  get 'managers/ads/:id', to: 'managers#ads_edit', as: 'managers_ads'
  patch 'managers/ads/:id', to: 'managers#ads_update'
  delete 'managers/ads/:id', to: 'managers#ads_delete'

  post 'managers/ad_img/:id', to: 'managers#img_update', as: 'managers_img'
  post 'managers/ad_toggle/:id', to: 'managers#ad_toggle', as: 'managers_ad_toggle'

  get 'managers/main'
  post 'managers/main_update/:type', to: 'managers#main_update'
  post 'managers/ad_check/:id', to: 'managers#ad_check'
  post 'managers/img_update/:id', to: 'managers#img_update'
  delete 'managers/ads_delete/:id', to: 'managers#ads_delete'
end
