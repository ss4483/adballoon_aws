class CreateAds < ActiveRecord::Migration[5.1]
  def change
    create_table :ads do |t|
      t.string :ad_type
      t.integer :main_type, default: 0

      t.string :company
      t.string :businessLicenseNumber
      t.string :phone

      t.string :title # 제목

      t.string :method # 응모 유형
      t.string :detail_method, default: [].to_yaml # 상세 응모 방법
      t.string :announced_link # 링크
      t.date :announced_date # 발표 날짜

      t.string :detail_prize, default: [].to_yaml # 경품
      t.string :link # 링크
      t.text :etc_message # 기타 메세지
      t.date :start_date # 시작 날짜
      t.date :end_date # 끝 날짜
      t.string :img # 이미지
      t.string :banner_img
      t.boolean :is_ok, default: false
      t.integer :views, default: 0
      
      t.string :temp_img
      t.string :temp_banner_img

      t.integer :banner_frequency # (0, 1) (single, multiple)
      t.date :banner_date

      t.integer :recommend_frequency # (0, 1) (single, multiple)
      t.date :recommend_date
      
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
