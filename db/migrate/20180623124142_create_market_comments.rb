class CreateMarketComments < ActiveRecord::Migration[5.1]
  def change
    create_table :market_comments do |t|

      t.boolean :isSecret
      t.text :content

      t.integer :reply_comment_id

      t.references :market_post, foreign_key: true
      
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
