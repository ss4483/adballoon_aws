class CreateJoinTableAdHashtag < ActiveRecord::Migration[5.1]
  def change
    create_join_table :ads, :hashtags do |t|
      # t.index [:ad_id, :hashtag_id]
      # t.index [:hashtag_id, :ad_id]
    end
  end
end
