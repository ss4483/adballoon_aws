class CreateMarketPosts < ActiveRecord::Migration[5.1]
  def change
    create_table :market_posts do |t|
      
      t.string :post_type
      t.string :title
      t.text :content
      t.string :img
      
      t.references :user, foreign_key: true
      
      t.timestamps
    end
  end
end
