# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180628063810) do

  create_table "ads", force: :cascade do |t|
    t.string "ad_type"
    t.integer "main_type", default: 0
    t.string "company"
    t.string "businessLicenseNumber"
    t.string "phone"
    t.string "title"
    t.string "method"
    t.string "detail_method", default: "--- []\n"
    t.string "announced_link"
    t.date "announced_date"
    t.string "detail_prize", default: "--- []\n"
    t.string "link"
    t.text "etc_message"
    t.date "start_date"
    t.date "end_date"
    t.string "img"
    t.string "banner_img"
    t.boolean "is_ok", default: false
    t.integer "views", default: 0
    t.string "temp_img"
    t.string "temp_banner_img"
    t.integer "banner_frequency"
    t.date "banner_date"
    t.integer "recommend_frequency"
    t.date "recommend_date"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_ads_on_user_id"
  end

  create_table "ads_hashtags", id: false, force: :cascade do |t|
    t.integer "ad_id", null: false
    t.integer "hashtag_id", null: false
  end

  create_table "hashtags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "identities", force: :cascade do |t|
    t.integer "user_id"
    t.string "provider"
    t.string "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_identities_on_user_id"
  end

  create_table "likes", force: :cascade do |t|
    t.integer "user_id"
    t.integer "ad_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ad_id"], name: "index_likes_on_ad_id"
    t.index ["user_id"], name: "index_likes_on_user_id"
  end

  create_table "market_comments", force: :cascade do |t|
    t.boolean "isSecret"
    t.text "content"
    t.integer "reply_comment_id"
    t.integer "market_post_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["market_post_id"], name: "index_market_comments_on_market_post_id"
    t.index ["user_id"], name: "index_market_comments_on_user_id"
  end

  create_table "market_posts", force: :cascade do |t|
    t.string "post_type"
    t.string "title"
    t.text "content"
    t.string "img"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_market_posts_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "user_type", default: "default"
    t.string "company"
    t.string "businessLicenseNumber"
    t.string "phone"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
