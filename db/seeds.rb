# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


email = ["b@b", "c@c", "d@d", "e@e", "f@f", "g@g"]
email.each do |e|
  User.create email: e, password: "123123"
end
hashtags = [ "치킨", "아이스크림", "아메리카노", "커피", "세탁기", "노트북", "믹서기", "영화", "블루베리", "댓글", "", "", "", ""]
banner_urls = [ 
                "https://postfiles.pstatic.net/MjAxODA3MDhfMjUz/MDAxNTMxMDU0MzczODU4.IPKr0pUjvYZuzxDkvF0pfL2_ug5TiHyfrmumTl3A2ewg.Y5BUMG8gcQrkUUftOZJt0KYpc45gGHt-hZ6x3FISntMg.PNG.ss4483/main.png?type=w773",
                "https://postfiles.pstatic.net/MjAxODA3MDhfMjU4/MDAxNTMxMDU0Mzc0MDEx.rRINn3Qx5eOIJvbuzFBMeb1hdRyJTh734hMS2YZhVm4g.f5-yuxCGK1uRIADbbuulVVrDtNLvZqO-UM9QrX4ncbUg.PNG.ss4483/main2.png?type=w773",
                "https://postfiles.pstatic.net/MjAxODA3MDhfMTI3/MDAxNTMxMDU0MzczODA0.drJSOLCxvjYau9hwgRc3xZd3FzkDcB6UsowCoOBq8J0g.zcmQPUvA_S5PKUIko0ilUz1LF9tKHHI5ftdzRyn5W0wg.PNG.ss4483/main3.png?type=w773"
              ]
img_urls = [ 
             "https://postfiles.pstatic.net/MjAxODA3MDhfMjU5/MDAxNTMxMDMzMDgyNjE2.NxC07dmPSoDie8-wlFLlM6NBq3vDggDyhubG8g86q-og.7iUQ3D1VEkfEzQTA3MBZo07-YiN3a3pr34j3QzVT9RYg.JPEG.ss4483/KakaoTalk_20180513_163408898.jpg?type=w773",
             "https://postfiles.pstatic.net/MjAxODA3MDhfMTQy/MDAxNTMxMDMzMDgyNjMz.7qewhr34TvgQsEGI3ihdZjfjUj0nlg_bD-Djf_-upS0g.zw2dTNDqbEneAapytxiYENkmjJJpAV1Tnr7tvUsC-VQg.PNG.ss4483/%EB%8C%93%EA%B8%80.png?type=w773",
             "https://postfiles.pstatic.net/MjAxODA3MDhfMzYg/MDAxNTMxMDMzMDgyOTE3.3mLrRSFeWD6A0A-ypTg4SEaE1FIwdP5CEeWxuDcD-jUg.cgNYnJ7JF3bTWV3V7F4E_DWzIFRp8Ji1oSNi7oaXUMkg.PNG.ss4483/%EB%B8%94%EB%A3%A8%EB%B2%A0%EB%A6%AC.png?type=w773",
             "https://postfiles.pstatic.net/MjAxODA3MDhfNjYg/MDAxNTMxMDMzMDgyOTE0.qvS5_079EVPOFTBzUp1WyDbQ1NUjTxAwXZyFDvwBxIIg.gXv8X1QdIiOsXeNE93RBLETHr7x8iLH_VMjzMHtd6_8g.PNG.ss4483/%EB%B9%A8%EB%9E%98.png?type=w773",
             "https://postfiles.pstatic.net/MjAxODA3MDhfMjQw/MDAxNTMxMDMzMDgyOTA0.VdCECDPHxfIPj0fjfx5c8NBTrf96rL79nM9IY79suI0g.0qMJFQRsAT3VSE_eSn1EpjVTH4iDGr-TMvB6LsS6yKYg.PNG.ss4483/%EC%97%AC%ED%96%89.png?type=w773",
             "https://postfiles.pstatic.net/MjAxODA3MDhfMjcz/MDAxNTMxMDMzMDgyODY5.tLZdh033KGPlW2tGAhXiDVXiVd3nKQk8yplHtXoWSv4g.FhJaNJ3SoEyAKmR5UeGv15DX6p1U33RLdPAGg7DUfYAg.PNG.ss4483/%EC%9A%B0%ED%91%9C.png?type=w773",
             "https://postfiles.pstatic.net/MjAxODA3MDhfMTIw/MDAxNTMxMDMzMDgzMDUz.OJh_9eJCl4MifxLMAS2tFxsRRwE6if2kSt3XEG5DYesg.W5rANPKwDVMRdwFMhvO1a2tOleLpvh4bPBUUANJzWCMg.PNG.ss4483/%EC%B4%88%EC%BD%9C%EB%A6%BF.png?type=w773" 
           ]

clients = [ 
            { company: "samsung", phone: "01015249845" }, 
            { company: "LG", phone: "01023487954" }, 
            { company: "클럽 클리오", phone: "01054970201" }, 
            { company: "KB 손해보험", phone: "01075461205" }, 
            { company: "신세계몰", phone: "01075896421" }, 
            { company: "SSG닷컴", phone: "01002458794" }, 
            { company: "채움스케치", phone: "01057153694" }
          ]
titles = [ '준비 중...', '우리 집 귀뚜라미를 자랑해주세요', '경.대.홍 with 뱅쇼', '제로다이 테렉스 투 보아', '리츠스타그램 이벤트','완벽한 피크닉 장소 알려주면, 피크닉 세트 쏜다' ]
methods = ["댓글", "설문", "인증샷", "퀴즈"]
detail_methods = [ 
                    [ "현재 게시물 좋아요공유응원댓글친구소환", "4월 28일 토요일 저녁 7시 50분 내몸사용설명서 시청", "시청 중 강현영 원장님을 사진 촬영하여 댓글로 본방사수 인증샷 남긴다." ],
                    [ "랄라블라 페이스북 좋아요 누르고", "마몽드 신제품 초성 \"ㄹㅈㅇㅌㅌㄴ\"맞히기", "친구한테 소식 알리면 더 좋음'" ],
                    [ "첫번째 경남은행대학생홍보대사 페이스북 페이지 팔로우 꾹", "두번째 해당 게시물 좋아요 꾹", "세번째 댓글에 아래 퀴즈 정답/ 쌓여있는 과제와 넘치는 시험범위로 힘들어하는 나의 친구홍길동 소환 후,응원의 한마디" ]
                  ]
detail_prizes = [
                  [ "치킨", "문화상품권 5천원" ],
                  [ "노트북", "치킨", "아메리카노" ],
                  [ "블루베리 1Box" ],
                  [ "왕복 항공권, 2박 숙박권" ],
                  [ "세탁기", "믹서기", "아메리카노" ]
                ]
etc_messages = [ "여러분의 취향을 댓글로 공유해주세요!\n추첨을 통해 취향에 맞는 커피를 드립니다'",
                 "다른 사람들을 배려하고\n사랑하는 반려견도 보호하는 방법!\n반려견과 외출할 때 지켜야 할 에티켓을 알아보고 \n 공유해주시면 추첨을 통해 선물을 드립니다.",
                 " FREE-PASS란?\n덕수궁 입장권, 유료 프로그램 체험권, FB 쿠폰 등 빵빵한 혜택!",
                 "", "", "", "",
                ]

50.times do 
  ad = Ad.new
  ad.businessLicenseNumber = 000000000
  client = clients.sample
  ad.company = client[:company]
  ad.phone = client[:phone]

  ad.ad_type = ["event", "coupon"].sample
  ad.title = titles.sample
  ad.method = methods.sample
  ad.link = "https://www.google.co.kr/"
  ad.detail_method = detail_methods.sample
  ad.remote_img_url = img_urls.sample
  ad.remote_temp_img_url = img_urls.sample if [true, false, false, false].sample
  ad.user = User.all.sample
  ad.user.user_type = "client" 
  ad.user.company = ad.company
  ad.user.phone = ad.phone
  ad.user.businessLicenseNumber = ad.businessLicenseNumber
  ad.start_date = Time.now - (1..5).to_a.sample*([true, false].sample ? 1 : -1).days
  ad.end_date = ad.start_date + (7..15).to_a.sample.days

  ad.detail_prize = detail_prizes.sample

  if ad.ad_type == "event"
    ad.announced_link = "https://www.google.co.kr/"
    ad.announced_date = ad.end_date + (1..5).to_a.sample.days
    ad.etc_message = etc_messages.sample
  else
    ad.etc_message = etc_messages.reject(&:empty?).sample 
  end
  ad.is_ok = [true, false].sample
  ad.save

  hashtags.sample(4).reject(&:empty?).each do |a|
    hashtag = Hashtag.find_or_create_by(name: a) 
    ad.hashtags << hashtag  if not ad.hashtags.include?(hashtag)
  end
end


Ad.all.sample(6).each do |ad|
  ad.recommend_frequency = 0
  ad.recommend_date = ad.start_date + (0..6).to_a.sample.days
  ad.save
end

Ad.all.sample(3).each_with_index do |ad, i|
  ad.remote_banner_img_url = banner_urls[i]
  ad.remote_temp_banner_img_url = banner_urls[2] if i == 0
  ad.banner_frequency = 0
  ad.banner_date = ad.start_date
  ad.save
end

Ad.where(is_ok: true, ad_type: "event").sample.update(main_type: 1)
Ad.where(is_ok: true, ad_type: "coupon").sample.update(main_type: 2)
Ad.where(is_ok: true, main_type: 0).sample.update(main_type: 3)


10.times do 
  User.create(email: Faker::Internet.email, password: "123123")
end

27.times do
  MarketPost.create(
    post_type: ["sell", "buy"].sample,
    title: Faker::Lorem.sentence, 
    content: Faker::Lorem.paragraph(10),  
    img: Faker::LoremPixel.image,
    user_id: [*7..16].sample
  )
end

34.times do
  c = MarketComment.new
  c.content = Faker::Lorem.sentence
  c.market_post_id = [*1..27].sample
  c.user_id = [*7..16].sample
  c.save
end 

50.times do
  comment = MarketComment.all.sample
  c = MarketComment.new
  c.content = Faker::Lorem.sentence
  c.reply_comment_id = comment.id
  c.market_post_id = comment.market_post_id
  c.user_id = [*7..16].sample
  c.save
end 

User.create email: "a@a", password: "123123", user_type: 'manager'
